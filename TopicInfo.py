from Expo.Components.Interfaces.Module import Module
from Expo.Exhibit import Exhibit


class TopicInfo:
    exhibit: Exhibit
    module: Module
    topic: str
    qos: int
    func: callable

    def __init__(self, exhibit: Exhibit, module: Module, topic: str, qos: int, func: callable) -> None:
        super().__init__()
        self.exhibit = exhibit
        self.module = module
        self.topic = topic
        self.qos = qos
        self.func = func
