from typing import Dict, List

from Configures.Mqtt.BrokerConfigData import BrokerConfigData
from Configures.Config import Config
from Configures.Mqtt.MQTTClientData import MQTTClientData
from Configures.Mqtt.MQTTConfigData import MQTTConfigData
from Expo.Components.Hardware.CPUTempModule import CPUTempModule
from Expo.Components.FloatRandomModule import FloatRandomModule
from Expo.Components.Hardware.HDDSpaceModule import HDDSpaceModule
from Expo.Components.Printer.PrinterStatusModule import PrinterStatusModule
from Expo.Components.StatusModuleLayer import StatusModuleLayer
from Expo.Components.VDNH_ZKH.StatusModuleLayerJsonned import StatusModuleLayerJsonned
from Expo.Components.VDNH_ZKH.VolumeModuleJsonned import VolumeModuleJsonned
from Expo.Exhibit import Exhibit
from TopicFormatter.TopicTemplate import TopicTemplate


class RegistredTypesBD:
    types: List[type] = [
        Config,
        BrokerConfigData,
        MQTTConfigData,
        MQTTClientData,
        Exhibit,
        CPUTempModule,
        FloatRandomModule,
        HDDSpaceModule,
        StatusModuleLayer,
        PrinterStatusModule,
        TopicTemplate,
        StatusModuleLayerJsonned,
        VolumeModuleJsonned
    ]
    types_dict: Dict[str, type] = None

    @classmethod
    def get_types(cls) -> Dict[str, type]:
        if cls.types_dict is None:
            cls.types_dict = RegistredTypesBD.convert_types_list(cls.types)
        return cls.types_dict

    @staticmethod
    def convert_types_list(types: List[type]) -> Dict[str, type]:
        return dict([(t.__name__, t) for t in types])

    @staticmethod
    def convert_data_to_object(data: object) -> object:
        return RegistredTypesBD.convert_type_and_params_to_object(
            RegistredTypesBD.get_types()[data['type']],
            data['params'] if 'params' in data.keys() else None
        ) if isinstance(data, Dict) and 'type' in data.keys() and data['type'] in RegistredTypesBD.get_types() \
            else dict(
            [(RegistredTypesBD.convert_data_to_object(k), RegistredTypesBD.convert_data_to_object(v)) for k, v in data.items()]
        ) if isinstance(data, Dict) \
            else [RegistredTypesBD.convert_data_to_object(v) for v in data] if isinstance(data, List) \
            else data

    @staticmethod
    def convert_type_and_params_to_object(t: type, params: Dict[str, object]) -> object:
        kvargs = dict([(k, RegistredTypesBD.convert_data_to_object(v)) for k, v in params.items()])
        try:
            return t(**kvargs)
        except Exception as err:
            raise RuntimeError('While extracting type {} from config has except "{}"'.format(t.__name__, err))
