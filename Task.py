from asmuron_lib.payload import Topic


class Task:
    topic: str
    payload: str

    def __init__(self, topic: str, payload: str):
        self.topic = topic
        self.payload = payload
        pass

    pass
