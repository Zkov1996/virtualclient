import struct

class Topic():
  def __init__(
    self
    , entryClass: str
    , expoTopic: str
    , nodeTopic: str
    , dataType: str
    , entryTopic: str
  ):
    self.entryClass = entryClass
    self.expoTopic = expoTopic
    self.nodeTopic = nodeTopic

    # self.fullNodeTopic = expoTopic  #fullTopic
    if nodeTopic:
      self.fullNodeTopic = expoTopic + '/' + nodeTopic
    else:
      self.fullNodeTopic = expoTopic

    self.dataType = dataType.upper()
    self.entryTopic = entryTopic

  @classmethod
  def fromFullNodeTopic(
    cls
    , fullNodeTopic: str
    , entryClass: str
    , dataType: str
    , entryTopic: str
  ):
    tp = fullNodeTopic.split('/')
    subNode = tp[1] if len(tp) > 1 else None
    return Topic(entryClass, tp[0], subNode, dataType, entryTopic)

  @classmethod
  def fromMqttTopic(cls, topic: str):
    tp = topic.split('/')
    if tp[0]!='ASMU' or (len(tp) != 5 and len(tp) != 6):
      return None

    if tp[1]!='CMD' and tp[1]!='METRIC':
      return None
    
    entryClass = tp[1]
    expoTopic = tp[2]
    nodeTopic = None
    if len(tp) == 6:
      nodeTopic = tp.pop(3)

    dataType = tp[3]
    entryTopic = tp[4]
    return Topic(entryClass, expoTopic, nodeTopic, dataType, entryTopic)

  def fold(self):
    return 'ASMU/{0}/{1}{2}/{3}/{4}'.format(
      self.entryClass, self.expoTopic
      , '' if self.nodeTopic is None else '/' + self.nodeTopic
      , self.dataType
      , self.entryTopic
    )

  # def copy(self):
  #   return Topic.fromMqttTopic(self.fold())


def decodeString(type, payload):
  return payload.decode('utf-8')

def parseBoolStr(valueStr):
  value = valueStr.lower()
  return True if value=='1' or value=='t' or value=='true' or value=='on' else False

def decodeBool(type, payload):
  if type == 'BOOL_S': 
    value = payload.decode('utf-8')
    return parseBoolStr(value) #True if value=='1' or value=='t' or value=='true' or value=='on' else False

  if type == 'BOOL' and len(payload) in [ 1, 2, 4, 8 ]:
    value = int.from_bytes(payload, byteorder='little')
    return True if value != 0 else False

  raise ValueError('decodeBool > ' + type)

def decodeInt(type, payload):
  if type == 'INT_S':
    return int(payload.decode('utf-8'))

  if len(payload) in [ 1, 2, 4, 8]:
    if type == 'INT_BE':  return int.from_bytes(payload, byteorder='big', signed=True)
    if type == 'INT_LE':  return int.from_bytes(payload, byteorder='little', signed=True)

  raise ValueError('decodeInt > ' + type)


def decodeFloat(type, payload):
  if type == 'FLOAT_S': 
    return float(payload.decode('utf-8'))

  if type == 'FLOAT_LE' and len(payload)==4:
    return struct.unpack('<f', payload)

  if type == 'FLOAT_LE' and len(payload)==8:
    return struct.unpack('<d', payload)

  if type == 'FLOAT_BE' and len(payload)==4:
    return struct.unpack('>f', payload)

  if type == 'FLOAT_BE' and len(payload)==8:
    return struct.unpack('>d', payload)

  raise ValueError('decodeInt > ' + type)
  # except: pass;
  # print("decodeFloat.XCPT", type, payload)
  # return None


###############################################
def payloadString(type, valueStr):
  return valueStr.encode('utf-8')

  # except: pass;
  # print("payloadString.XCPT", type, valueStr)
  # return None

def payloadBool(type, valueStr):
  boolVal = valueStr=='1' or valueStr=='t' or valueStr=='true' or valueStr=='on'

  if type == 'BOOL_S':
    return '1' if boolVal else '0'

  if type == 'BOOL':
    return struct.pack("b", boolVal)

  raise ValueError('payloadBool > ' + type)

def payloadInt(type, valueStr):
  if type == 'INT_S':
    return valueStr.encode('utf-8')

  intVal = int(valueStr)
  if type == 'INT_BE':  return (intVal).to_bytes(8, byteorder='big', signed=True)
  if type == 'INT_LE':  return (intVal).to_bytes(9, byteorder='little', signed=True)

  raise ValueError('payloadInt > ' + type)

def payloadFloat(type, valueStr):
  if type == 'FLOAT_S': 
    return valueStr.encode('utf-8')

  floatVal = float(valueStr)
  if type == 'FLOAT_LE':
    return struct.pack("<d", floatVal)

  if type == 'FLOAT_BE':
    return struct.unpack('>d', floatVal)

  raise ValueError('payloadFloat > ' + type)

