import os
import sys
import time
from . import conf
from datetime import datetime, date

incindentsDir = conf.incindentsDir()
logGranulaSize = conf.getInt('LOG_GRANULA', 10) # MB
logGranulaAge  = conf.getInt('LOG_AGE', 30) # Days
verboseLog = conf.getBool('APP_DEBUG', False)

def debug(line):
  if verboseLog:
    if not line.endswith("\n"):
      line += "\n"

    sys.stdout.write('... ' + line)
    sys.stdout.flush()

def info(line):
  if not line.endswith("\n"):
    line += "\n"

  sys.stdout.write(line)
  sys.stdout.flush()

def error(line):
  if not line.endswith("\n"):
    line += "\n"

  sys.stderr.write('ERROR: ' + line)
  sys.stderr.flush()

def reportIncindent(topic, msg):
  try:
    fileName = incindentsDir + '/' + topic
    with open(fileName, 'w') as f:
      if msg:
        f.write(msg)
  except Exception as ex:
    error('XCPT.reportIncindent: ' + str(ex))

def xcpt(where, e):
  msg = str(e)
  error("{0}.XCPT: ".format(where) + msg)
  reportIncindent('xcpt.' + where, msg)

def xcptOs(where, e):
  msg = "{0}.XCPT: errno {1}, error {2}, filename {3}".format(where, e.errno, e.strerror, e.filename)
  error(msg)
  reportIncindent('xcptos.' + where, msg)

#-------------------------------#

class LogFile():
  def __init__(self, scriptName, dupStdout = False, verbose = False):
    self.dirName = conf.logDir()
    baseName = os.path.basename(scriptName)
    self._baseName = os.path.splitext(baseName)[0] + '.log'
    self._fileName = self.dirName + '/' + self._baseName
    self._hFile = open(self._fileName, "ab")
    self._dupStdout = dupStdout
    self._verboseLog = verbose

  def _rotate(self):
    close_fh = self._hFile
    self._hFile = sys.stdout
    close_fh.close()

    try:
      portionFileName = self._fileName + '.' + date.today().isoformat()
      
      counter = 1
      if os.path.isfile(portionFileName):
        while os.path.isfile(portionFileName + '.' + str(counter).rjust(3, '0')):
          counter += 1

        portionFileName = portionFileName + '.' + str(counter).rjust(3, '0')

      os.rename(self._fileName, portionFileName)
      self._hFile = open(self._fileName, "ab")

    except OSError as e:
      xcptOs("log.rotate", e)

  def _cleanup(self):
    try:
      for fn in os.listdir(self.dirName):
        if fn.startswith(self._baseName):
          testFile = self.dirName + '/' + fn
          fmtm = os.path.getmtime(testFile)
          if (time.time() - fmtm) > logGranulaAge*24*3600:
            os.unlink(testFile)

    except OSError as e:
      xcptOs("log.cleanup", e)

  def _log(self, lineIn, level):
    try:
      tstr = datetime.now().isoformat(sep=' ').split('.')[0] #, timespec='seconds'
      if level < 0:
        tstr += ' E'
      elif level > 0:
        tstr += ' .'
      elif level == 0:
        tstr += '  '

      tstrBuf = (tstr + ' : ').encode('utf8')

      try:
        lineStr = lineIn.decode("utf8")
      except (UnicodeDecodeError, AttributeError): 
        lineStr = lineIn

      if not lineStr.endswith("\n"):
        lineStr += "\n"

      try:
        lineBuf = lineStr.encode('utf8')
      except (UnicodeDecodeError, AttributeError): pass

      self._hFile.write(tstrBuf) 
      self._hFile.write(lineBuf)
      self._hFile.flush()

      if self._dupStdout:
        if level < 0:
          error(lineStr)
        elif level > 0:
          debug(lineStr)
        else:
          info(lineStr)

      if self._hFile.tell() > logGranulaSize * 1000*1000:
        self._rotate()
        self._cleanup()

    except IOError as e:
      xcpt("log", e)

  def debug(self, line):
    if self._verboseLog:
      self._log(line, 1)

  def info(self, line):
    self._log(line, 0)

  def error(self, line):
    self._log(line, -1)

  def xcpt_os(self, where, e):
    msg = "{0}.OS_XCPT: errno {1}, error {2}, filename {3}\n".format(where, e.errno, e.strerror, e.filename)
    self._log(msg, -1)
    reportIncindent('xcpt.' + self._baseName + '.' + where, msg)

  def xcpt(self, where, e):
    msg = "{0}.XCPT".format(where) + " : " + str(e) + "\n"
    self._log(msg, -1)
    reportIncindent('xcptos.' + self._baseName + '.' + where, msg)
