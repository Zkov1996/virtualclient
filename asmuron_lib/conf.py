import os
import configparser

__rootPathDir = ''
__conf = None

def getStr(KEY, defa = None) -> str:
  res = __conf.get(KEY, defa).strip()
  if res.startswith('"') and res.endswith('"'):
    res = res.strip('"')
  return res

def getBool(KEY, defa = None) -> bool:
  value = __conf.get(KEY, '').lower()
  if not value:
    if defa == None:
      raise AttributeError('getBool without Default')

    return defa
  else:
    return True if value=='1' or value=='t' or value=='true' or value=='on' else False

def getInt(KEY, defa = None) -> int:
  strVal = __conf.get(KEY, '')
  if not strVal:
    if defa == None:
      raise AttributeError('getInt without Default')

    return defa
  else:
    return int(strVal)

def getFloat(KEY, defa = None) -> float:
  strVal = __conf.get(KEY, '')
  if not strVal:
    if defa == None:
      raise AttributeError('getFloat without Default')

    return defa
  else:
    return float(strVal)

def rootPath():
  return __rootPathDir

def getCID():
  cid = getInt('CID', None)
  if cid:
    return cid
  else:
    raise AttributeError('CID must be defined in .env')


def logDir():
  logDirVal = getStr('LOG_DIR', __rootPathDir + '/log')
  if logDirVal.startswith('./'):
    logDirVal = __rootPathDir + logDirVal[1:]
  return logDirVal

def incindentsDir():
  return logDir() + '/incindents'

def brokerHost():
  # return getStr('BROKER_HOST', 'broker.asmu.polytech.one')
  return getStr('BROKER_HOST', 'localhost')
  # return getStr('BROKER_HOST', '89.175.188.125')

def brokerPort():
  return getInt('BROKER_PORT', 1883)

def publicFilePath(fileDest):
  if fileDest.startswith('/'):
    return fileDest
  else:
    return rootPath() + '/public/'+ fileDest

# ----------------------------------------- #
def __loadConf():
  pp = os.path.realpath(os.path.realpath(__file__)).replace('\\', '/').split('/')
  while len(pp) > 0:
    confFile = '/'.join(pp) + '/.env'
    if os.path.isfile(confFile):
      break
    pp.pop()

  if not os.path.isfile(confFile):
    raise Exception('No config File: .env')
  
  configParser = configparser.ConfigParser(strict=False, default_section='CONFIG')
  configParser.read(confFile)

  global __rootPathDir, __conf
  __rootPathDir = os.path.dirname(confFile)
  __conf = configParser['CONFIG']

  # if not os.path.isdir(logDir()):
  #   raise Exception('No LOG DIR: ' + logDir())

if not __conf:
  __loadConf()
