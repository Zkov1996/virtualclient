import queue
import requests
from threading import Thread, Event
from . import conf #, log
from .payload import Topic

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

iridiumSrv = conf.getStr('IRIDIUM_SRV')
iridiumThreads = conf.getInt('IRIDIUM_THREADS', 1)

# from typing import List

# --------------------- #
class IridiumTask():
  def __init__(self, node_uid: str, topic: Topic, iridium_node: str, command: str, addr: str, mac: str) -> None:
    self.node_uid = node_uid
    self.topic = topic
    self.addr = addr
    self.mac = mac
    self.command = command
    self.iridium_node = iridium_node

    uri = iridiumSrv + iridium_node
    if not '?' in iridium_node:
      uri += '?'
    else:
      uri += '&'

    uri += 'command=' + command

    if not '${addr}' in uri:
      uri += '&ip=${addr}'

    # https://127.0.0.1:5000/api/v1/iridium/devices/pjlink?ip=192.168.200.198&command=on
    uri = uri.replace('${addr}', addr).replace('${mac}', mac)
    self.uri = uri

# --------------------- #
class IridiumReport():
  def __init__(self, task: IridiumTask, status: str, statusDesc: str) -> None:
    self.task = task
    self.status = status
    self.statusDesc = statusDesc

# --------------------- #
class IridiumWorker():
  def __init__(self, termEvent: Event, requestTimeout: int):
    self.termEvent = termEvent
    self.reportQueue = queue.Queue()
    self.iridiumQueue = queue.Queue()
    self.requestTimeout = requestTimeout
    self.__threads = []

  def queueTask(self, task: IridiumTask, reportWaitingStatus: bool):
    status = (
      'UP' if task.command=='on' else (
      'DOWN' if task.command=='off' else None
    ))

    if reportWaitingStatus and status:
      report = IridiumReport(task, status, None)
      self.reportQueue.put(report)

    self.iridiumQueue.put(task)

  def __reportStatus(self, task: IridiumTask, status: str, statusDesc: str):
    report = IridiumReport(task, status, statusDesc)
    self.reportQueue.put(report)

  def __reportFail(self, task: IridiumTask, statusDesc: str):
    report = IridiumReport(task, 'ERROR', statusDesc)
    self.reportQueue.put(report)

  def __iridiumWorker(self):
    while not self.termEvent.wait(0):
      try:
        task: IridiumTask = self.iridiumQueue.get(timeout=0.5)

        try:
          # print('...... iridium_task. URI:' + task.uri)
          respo = requests.get(task.uri, verify=False, timeout=self.requestTimeout)
          # print('......... respoCode: ' + str(respo.status_code))

          if respo.status_code == 200:
            respoData = respo.json()
            # print('......... respoData', str(respoData))

            if respoData:
              respoDevice = respoData['devices']  if ('devices' in respoData) else {}
              respoStatus = respoDevice['status'] if ('status' in respoDevice) else '<NULL>'

              status = None
              if respoStatus=='on':         status = 'ON'
              elif respoStatus=='off':      status = 'OFF'
              elif respoStatus=='offline':  status = 'OFFLINE'
              elif respoStatus=='error':    status = 'ERROR'

              if status:
                respoMsg = respoDevice['message'] if ('message' in respoDevice) else None
                self.__reportStatus(task, status, respoMsg)
              else:
                self.__reportStatus(task, 'ERROR', 'Bad Iridium STATUS:' + respoStatus)

            else:
              self.__reportFail(task, 'Empty response')
          else:
            self.__reportFail(task, 'Bad resopnse code: ' + respo.status_code)

        except requests.ConnectionError as ex:
          # self.__reportFail(task, 'ConnectionError')
          self.__reportStatus(task, 'UNKNOWN', 'Iridium connection failed: ' + task.uri)  # + str(ex)
        except requests.Timeout:
          # self.__reportFail(task, 'Timeout')
          self.__reportStatus(task, 'UNKNOWN', 'Iridium Timeout:' + str(self.requestTimeout) + ' sec')
        except Exception as ex:
          self.__reportFail(task, 'XCPT: ' + str(ex))

      except Exception:
        pass

  def start(self):
    for i in range(iridiumThreads):
      t = Thread(target=self.__iridiumWorker); i
      t.start()
      self.__threads.append(t)

  def finish(self):
    for thread in self.__threads:
      thread.join()

    try:
      while True:
        task: IridiumTask = self.iridiumQueue.get(timeout=0)
        self.__reportFail(task, 'Aborted')
    except Exception: pass

