import psycopg2
from . import conf

class PgDb():
  def __init__(self, mode = None):
    self.dbcon = None
    self.open(mode)

  def isAlive(self):
    try:
      self.call('webResume2', 0)
      return True
    except Exception:
      return False

  def open(self, mode):
    self.close()

    if mode == 'local':
      dbcon = psycopg2.connect(
        host = conf.getStr('DB_LOCAL_HOST', 'localhost')
        , port = conf.getInt('DB_LOCAL_PORT', 5432)
        , database = conf.getStr('DB_LOCAL_DATABASE', 'asmuron_local')
        , user = conf.getStr('DB_LOCAL_USERNAME')
        , password = conf.getStr('DB_LOCAL_PASSWORD')
      )
    elif mode == 'main':
      dbcon = psycopg2.connect(
        host = conf.getStr('DB_MAIN_HOST', 'localhost')
        , port = conf.getInt('DB_MAIN_PORT', 5432)
        , database = conf.getStr('DB_MAIN_DATABASE', 'asmuron_global')
        , user = conf.getStr('DB_MAIN_SYS_USERNAME')
        , password = conf.getStr('DB_MAIN_SYS_PASSWORD')
      )
    else:
      raise AssertionError('Invalid PgDb mode')
    
    if dbcon:
      dbcon.set_client_encoding('UTF8')
      dbcon.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
      self.dbcon = dbcon

  def close(self):
    if self.dbcon:
      self.dbcon.close()
      self.dbcon = None
    
  def call(self, procname, *args):
    cursor = self.dbcon.cursor()
    cursor.callproc('"'+procname+'"', args)
    results = cursor.fetchall()
    cursor.close()
    return results

  def callSingleRow(self, procname, *args):
      result = self.call(procname, *args)
      if len(result) != 1:
        raise ValueError('callSingleRow. row count != 1')

      return result[0]

  def callSingleValue(self, procname, *args):
      result = self.callSingleRow(procname, *args)
      if len(result) != 1:
        raise ValueError('callSingleValue. value count != 1')

      return result[0]
