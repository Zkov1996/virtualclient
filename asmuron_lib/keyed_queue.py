from threading import Event, RLock
import time

class KeyedQueue():
  def __init__(self):
    self.lock = RLock()
    self.hasMsgs = Event()
    self.keysQueue = []
    self.dicto = dict()
    

  def queue(self, key, item):
    with self.lock:
      if key in self.dicto:
        return self.dicto[key]

      self.keysQueue.insert(0, key)
      self.dicto[key] = item

    self.hasMsgs.set()
    return None

  def dequeue(self, timeout):
    stopClock = time.monotonic() + timeout
    remain = timeout

    while True:
      if self.hasMsgs.wait(remain) == True:
        with self.lock:
          if len(self.keysQueue):
            return self.keysQueue.pop()
          else:
            self.hasMsgs.clear()

      remain = stopClock - time.monotonic()
      if remain <= 0:
        return None

  def itemIsProcessing(self, key):
    with self.lock:
      if not key in self.dicto:
        return None

      return not key in self.keysQueue

  def peekItem(self, key):
    with self.lock:
      res = self.dicto[key] if key in self.dicto else None

    return res

  def removeItem(self, key):
    with self.lock:
      if key in self.keysQueue:
        self.keysQueue.remove(key)
      del self.dicto[key]

