
class Status():
  def __init__(self, status: str, desc: str):
    self.status = status
    self.desc = desc

  def fold(self):
    return self.status + (' ' + self.desc if self.desc is not None else '')

  @classmethod
  def fromStrings(cls, _status: str, desc: str):
    std = _status.split(maxsplit=1)
    status = std[0].upper()
    if status not in [ 'ON', 'OFF', 'UP', 'DOWN', 'ERROR', 'SERVICE', 'UNKNOWN', 'OFFLINE' ]:
      return Status('UNKNOWN', desc)
    return Status(status, desc)

  @classmethod
  def composeStatusList(cls, _statuses):
    statuses = list(filter(lambda st: st is not None, _statuses))

    errors, on, off, up, down, srv, offline, unknown = 0, 0, 0, 0, 0, 0, 0, 0
    for st in statuses:
      if   st.status == 'ON': on += 1
      elif st.status == 'OFF': off += 1
      elif st.status == 'ERROR': errors += 1
      elif st.status == 'UP': up += 1
      elif st.status == 'DOWN': down += 1
      elif st.status == 'SERVICE': srv += 1
      elif st.status == 'OFFLINE': offline += 1
      elif st.status == 'UNKNOWN': unknown += 1

    if srv > 0:  genStatus='SERVICE'
    elif errors > 0: genStatus='ERROR'
    elif offline > 0: genStatus='OFFLINE'
    elif up > 0 and down == 0: genStatus='UP'
    elif down > 0 and up == 0: genStatus='DOWN'
    elif off > 0: genStatus='OFF'
    elif on  > 0: genStatus='ON'
    else: genStatus = 'UNKNOWN'

    genDesc = None
    for st in statuses:
      if (st.status == genStatus) and (st.desc is not None):
        if genDesc is None:
          genDesc = ''
        else:
          genDesc += ' '

        genDesc += st.desc

    return Status(genStatus, genDesc)
