import time
import tempfile
import os
import tempfile

def isRunTime(category, timeIntervalSec):
  tmp = tempfile.gettempdir()
  fn = tmp + '/asmuron.IsTime.' + category + '.signal'
  if not os.path.exists(fn):
    open(fn, 'w').write(time.ctime())
    return True

  ft = os.path.getmtime(fn)
  gt = time.time()
  if ft + timeIntervalSec <= gt:
    open(fn, 'w').write(time.ctime())
    return True

  return False
