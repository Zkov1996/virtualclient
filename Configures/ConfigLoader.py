import json
from pathlib import Path

from Configures.Config import Config
from RegistredTypesBD import RegistredTypesBD


class ConfigLoader:
    @staticmethod
    def load(path: str) -> Config:
        configRawObj = json.loads(open(str(Path(path).absolute()), 'r').read())
        return RegistredTypesBD.convert_data_to_object(configRawObj)
