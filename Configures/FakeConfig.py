from pathlib import Path
from typing import List

from Configures.Config import Config
from Expo.Components.FloatRandomModule import FloatRandomModule
from Expo.Components.Hardware.CPUTempModule import CPUTempModule
from Expo.Components.Hardware.HDDSpaceModule import HDDSpaceModule
from Expo.Components.StatusModuleLayer import StatusModuleLayer
from Expo.Exhibit import Exhibit


class FakeConfig(Config):
    def __init__(self) -> None:
        super().__init__()

    # exhibits: List[Exhibit] = [
    #     Exhibit(
    #         'TST-001',
    #         [
    #             StatusModuleLayer('STATUS'),
    #             FloatRandomModule('FLOAT_01', 10, 20),
    #             FloatRandomModule('FLOAT_02', 10, 20),
    #             FloatRandomModule('FLOAT_03', 10, 20),
    #             FloatRandomModule('FLOAT_04', 10, 20),
    #             FloatRandomModule('FLOAT_05', 10, 20),
    #         ]
    #     )
    # ]


    exhibits: List[Exhibit] = [
        Exhibit(
            'TST-{:03}'.format(i),
            None,
            # [StatusModuleLayer('STATUS')]
            [StatusModuleLayer('STATUS', enabled_on_start=True)]
            + [FloatRandomModule('FLOAT_{:02}'.format(j), 10, 10) for j in range(1, 21)]
        )
        for i in range(1, 50)
    ]

    # exhibits: List[Exhibit] = [
    #     Exhibit(
    #         open(str(Path('ExpoTopic.txt').absolute()), 'r').read(),
    #         None,
    #         # [StatusModuleLayer('STATUS')]
    #         [
    #             # StatusModuleLayer('STATUS', enabled=True),
    #             StatusModuleLayer('STATUS'),
    #             HDDSpaceModule('HDD_Free', 'C:', 60),
    #             CPUTempModule('CPU_TEMP', 60),
    #         ]
    #     )
    # ]

    #     Exhibit(
    #         'TST-001',
    #         [
    #             StatusModuleLayer('STATUS'),
    #             FloatRandomModule('FLOAT_01', 10, 20),
    #             FloatRandomModule('FLOAT_02', 10, 20),
    #             FloatRandomModule('FLOAT_03', 10, 20),
    #             FloatRandomModule('FLOAT_04', 10, 20),
    #             FloatRandomModule('FLOAT_05', 10, 20),
    #         ]
    #     )
    # ]

