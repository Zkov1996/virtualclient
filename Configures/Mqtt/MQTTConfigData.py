from dataclasses import dataclass

from Configures.Mqtt.BrokerConfigData import BrokerConfigData
from Configures.Mqtt.MQTTClientData import MQTTClientData
from TopicFormatter.TopicTemplate import TopicTemplate


@dataclass
class MQTTConfigData:
    client: MQTTClientData
    broker: BrokerConfigData
    topic_template: TopicTemplate
