from dataclasses import dataclass


@dataclass
class MQTTClientData:
    device: str
