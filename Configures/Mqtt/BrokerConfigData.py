from dataclasses import dataclass


@dataclass
class BrokerConfigData:
    host: str
    port: int
