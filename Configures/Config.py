from dataclasses import dataclass
from typing import List

from Configures.Mqtt.MQTTConfigData import MQTTConfigData
from Expo.Exhibit import Exhibit


@dataclass
class Config:
    mqtt: MQTTConfigData
    exhibits: List[Exhibit]


