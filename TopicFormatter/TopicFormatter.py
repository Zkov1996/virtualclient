from TopicFormatter.TopicTemplate import TopicTemplate


class TopicFormatter:
    topic_template: TopicTemplate = None

    def __init__(self, topic_template: TopicTemplate) -> None:
        self.topic_template = topic_template

    # device node message direction
    def get(self, *args, **kvargs) -> str:
        return self.topic_template.template.format(*args, **kvargs)
        pass
