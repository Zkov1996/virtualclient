from dataclasses import dataclass


@dataclass
class TopicTemplate:
    template: str = None
