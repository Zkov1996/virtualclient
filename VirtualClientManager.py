import queue
import threading
from datetime import datetime
from threading import Event
from typing import List, Dict, Union

import paho.mqtt.client as mqtt

from TopicFormatter.TopicFormatter import TopicFormatter
from asmuron_lib import payload as Payload

from Configures import Config
from Decorators.method_link import method_link
from Expo.Components.Decorators.ModuleOnMessageDecorator import OnMessageInfo
from Expo.Components.Interfaces.Layer import Layer
from Expo.Components.Interfaces.Module import Module
from Expo.Exhibit import Exhibit
from Task import Task
from TopicInfo import TopicInfo


class VirtualClientManager:
    taskQueue = queue.Queue()
    termEvent: Event
    mqtt_client: mqtt.Client

    subscribeQueue = queue.Queue()
    # connected: bool = False

    topics: Dict[str, TopicInfo] = {}
    config: Config = None

    start_time: datetime = None

    topic_formatter: TopicFormatter = None

    def __init__(self, mqtt_client: mqtt.Client, config: Config, termEvent: Event) -> None:
        super().__init__()
        self.mqtt_client = mqtt_client
        self.config = config
        self.termEvent = termEvent
        self.topic_formatter = TopicFormatter(config.mqtt.topic_template)

    def start(self):
        if self.start_time is None:
            for exhibit in self.config.exhibits:
                self.subscribe_exhibit(exhibit)

            self.start_time = datetime.now()
            self._update_cycle()
            # self._module_update_tread()

            print("exiting, flushing remaining tasks")

    @method_link
    def on_connected(self, client, userdata, flags, rc):
        print("connected to broker, code:" + str(rc))

        # client.subscribe("ASMU/METRIC/+/+/+", 2)
        # client.subscribe("ASMU/METRIC/+/+/+/+", 2)
        # self._module_update_tread()
        # if :
        #     self._update_cycle()


    @method_link
    def on_message(self, client, userdata, msg):
        # ASMU/CMD/ABC-999/WEBKIOSK/TEXT/CONTENT_UPDATE_CONTROL
        # ASMU/CMD/{Parameter[0]}/TEXT/CONTENT_UPDATE_CONTROL
        try:
            # t = Payload.Topic.fromMqttTopic(msg.topic)
            print("receive in manager: {}".format(msg.topic))
            self.taskQueue.put(Task(msg.topic, Payload.decodeString("TEXT", msg.payload)))
        except Exception as e:
            print('on_message: ' + str(e))

    def _update_cycle(self):
        while not self.termEvent.wait(0):
            self._subscribe_thread()
            self._task_queue_thread()
            self._module_update_tread()
            pass
        pass

    def _task_queue_thread(self):
        while len(self.taskQueue.queue) > 0:
            task = self.taskQueue.get()
            topic: str = task.topic
            payload = task.payload
            if topic in self.topics.keys():
                ti = self.topics[topic]
                if self._calculate_receive_layers_ignore(ti.exhibit.get_layers(), ti.module, payload):
                    # Log.info('Ignore receive {}: {}'.format(topic, payload))
                    continue
                ti.func(ti.module, payload)
            pass

    def _module_update_tread(self):
        # mi = MessageInterface()

        def send(payload: Union[str, int, float, bool]):
            topic_value_type = None
            payload_converted = None

            if isinstance(payload, str):
                topic_value_type = 'TEXT'
                payload_converted = payload
                pass
            elif isinstance(payload, int):
                topic_value_type = 'INT_S'
                payload_converted = str(payload)
                pass
            elif isinstance(payload, float):
                topic_value_type = 'FLOAT_S'
                payload_converted = str(payload)
                pass
            elif isinstance(payload, bool):
                topic_value_type = 'BOOL_S'
                payload_converted = 'true' if payload else 'false'
                pass
            else:
                raise RuntimeError("Can`t send type {}".format(type(payload)), payload)

            topic = self.topic_formatter.get(device=exhibit.device, node=exhibit.node, message=module.name)
            # topic = 'ASMU/METRIC/{}/{}/{}'.format(exhibit.device, topic_value_type, module.name) if exhibit.node is None \
            #     else 'ASMU/METRIC/{}/{}/{}/{}'.format(exhibit.device, exhibit.node, topic_value_type, module.name)

            if self._calculate_send_layers_ignore(layers, module, payload):
                print('Ignore send {}: {}'.format(topic, payload_converted))
                return

            print('send {}: {}'.format(topic, payload_converted))
            self.mqtt_client.publish(topic, payload_converted)

            pass

        for exhibit in self.config.exhibits:
            layers = exhibit.get_layers()

            for module in exhibit.get_modules():
                if self._calculate_update_layers_ignore(layers, module):
                    continue
                module.update((datetime.now() - self.start_time).total_seconds(), send)
        pass

    def _calculate_send_layers_ignore(self, layers: List[Layer], module: Module, payload: str) -> bool:
        return any([not layer.pass_send_message(module, module.name, payload) for layer in layers])
        pass

    def _calculate_receive_layers_ignore(self, layers: List[Layer], module: Module, payload: str) -> bool:
        return any([not layer.pass_receive_message(module, module.name, payload) for layer in layers])
        pass

    def _calculate_update_layers_ignore(self, layers: List[Layer], module: Module) -> bool:
        return any([not layer.pass_module_update(module) for layer in layers])
        pass

    def subscribe_exhibit(self, exhibit: Exhibit):

        exhibit_modules: List[Module] = exhibit.get_modules()

        for module in exhibit_modules:
            commands: List[OnMessageInfo] = module.get_commands()
            for command in commands:
                self.subscribe_module_command(exhibit, module, command.topic, command.qos, command.func)
            pass
        pass

    # self.subscribe_module_func()

    def _subscribe_thread(self):
        while len(self.subscribeQueue.queue) > 0:
            ti = self.subscribeQueue.get()
            self._subscribe(ti)
        pass

    def subscribe_module_command(self, exhibit: Exhibit, module: Module, topic_command: str, qos: int, func: callable):

        # if exhibit.node is not None:
        #     # topic = "ASMU/CMD/{}/{}/{}".format(exhibit.device, exhibit.node, topic_command)
        #     topic = self.topic_formatter.get(device=exhibit.device, node=exhibit.node, message=module.name)
        #     ti = TopicInfo(exhibit, module, topic, qos, func)
        #     self.subscribeQueue.put(ti)
        # else:
        #     # topic = "ASMU/CMD/{}/{}".format(exhibit.device, topic_command)
        #     topic = self.topic_formatter.get(device=exhibit.device, node=exhibit.node, message=module.name)
        #     ti = TopicInfo(exhibit, module, topic, qos, func)
        #     self.subscribeQueue.put(ti)

        topic = self.topic_formatter.get(device=exhibit.device, node=exhibit.node, message=topic_command)
        ti = TopicInfo(exhibit, module, topic, qos, func)
        self.subscribeQueue.put(ti)

        # self.mqtt_client.subscribe("ASMU/CMD/{}/+/{}".format(expoName, local_topic), 2)
        pass

    def _subscribe(self, topic_info: TopicInfo):
        print("subscribe {}".format(topic_info.topic))
        self.topics[topic_info.topic] = topic_info
        self.mqtt_client.subscribe(topic_info.topic, topic_info.qos)
        # self.mqtt_client.subscribe(topic_info.topic, 0)
