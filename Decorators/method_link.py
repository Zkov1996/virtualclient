def method_link(fn):
    def wrapper(self, *args, **kwargs):
        return fn(self, *args, **kwargs)

    return wrapper
