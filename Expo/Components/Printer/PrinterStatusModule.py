import json
from typing import Union

import win32print

from Expo.Components.Interfaces.PeriodicModule import PeriodicModule
from Expo.Components.Printer.PRINTER_INFO_2 import PRINTER_INFO_2
from Expo.Components.Printer.PrinterStatus import PrinterStatus


class PrinterStatusModule(PeriodicModule):
    _period: float

    def __init__(self, name: str, period: float) -> None:
        super().__init__(name)
        self._period = period

    def _get_next_time_delta(self) -> float:
        return self._period

    def _on_triggered(self, message_send: callable):
        # val = self.get_printer_status()
        val = self.get_printer_attribute()
        if val is None:
            print('Can`t get printer state')
            return
        message_send(str(val).replace('PrinterStatus.', ''))
        pass

    def get_printer_status(self) -> Union[PrinterStatus, None]:
        printer_name = win32print.GetDefaultPrinter()
        if printer_name is None:
            return None
        printer_handle = win32print.OpenPrinter(printer_name)
        if printer_handle is None:
            return None
        printer_info_2 = PRINTER_INFO_2.from_printer(printer_handle)
        if printer_info_2 is None:
            return None
        return printer_info_2.get_status()

    def get_printer_attribute(self) -> Union[PrinterStatus, None]:
        printer_name = win32print.GetDefaultPrinter()
        if printer_name is None:
            return None
        printer_handle = win32print.OpenPrinter(printer_name)
        if printer_handle is None:
            return None
        printer_info_2 = PRINTER_INFO_2.from_printer(printer_handle)
        if printer_info_2 is None:
            return None
        return printer_info_2.get_attribute()
