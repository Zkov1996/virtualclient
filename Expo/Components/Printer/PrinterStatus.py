from enum import IntFlag
from typing import List

import win32print


class PrinterStatus(IntFlag):
    READY = 0,
    PAUSED = win32print.PRINTER_STATUS_PAUSED,
    ERROR = win32print.PRINTER_STATUS_ERROR,
    PENDING_DELETION = win32print.PRINTER_STATUS_PENDING_DELETION,
    PAPER_JAM = win32print.PRINTER_STATUS_PAPER_JAM,
    PAPER_OUT = win32print.PRINTER_STATUS_PAPER_OUT,

    MANUAL_FEED = win32print.PRINTER_STATUS_MANUAL_FEED,
    PAPER_PROBLEM = win32print.PRINTER_STATUS_PAPER_PROBLEM,
    OFFLINE = win32print.PRINTER_STATUS_OFFLINE,
    IO_ACTIVE = win32print.PRINTER_STATUS_IO_ACTIVE,
    BUSY = win32print.PRINTER_STATUS_BUSY,
    PRINTING = win32print.PRINTER_STATUS_PRINTING,
    OUTPUT_BIN_FULL = win32print.PRINTER_STATUS_OUTPUT_BIN_FULL,
    NOT_AVAILABLE = win32print.PRINTER_STATUS_NOT_AVAILABLE,
    WAITING = win32print.PRINTER_STATUS_WAITING,

    INITIALIZING = win32print.PRINTER_STATUS_INITIALIZING,
    DOOR_OPEN = win32print.PRINTER_STATUS_DOOR_OPEN,
    NO_TONER = win32print.PRINTER_STATUS_NO_TONER,
    OUT_OF_MEMORY = win32print.PRINTER_STATUS_OUT_OF_MEMORY,
    PAGE_PUNT = win32print.PRINTER_STATUS_PAGE_PUNT,
    POWER_SAVE = win32print.PRINTER_STATUS_POWER_SAVE,
    PROCESSING = win32print.PRINTER_STATUS_PROCESSING,
    SERVER_UNKNOWN = win32print.PRINTER_STATUS_SERVER_UNKNOWN,
    TONER_LOW = win32print.PRINTER_STATUS_TONER_LOW,
    USER_INTERVENTION = win32print.PRINTER_STATUS_USER_INTERVENTION,
    WARMING_UP = win32print.PRINTER_STATUS_WARMING_UP
