from dataclasses import dataclass

import win32print

from Expo.Components.Printer.PrinterAttribute import PrinterAttribute
from Expo.Components.Printer.PrinterStatus import PrinterStatus


@dataclass
class PRINTER_INFO_2:
    pServerName: str  # 00
    pPrinterName: str  # 01
    pShareName: str  # 02
    pPortName: str  # 03
    pDriverName: str  # 04
    pComment: str  # 05
    pLocation: str  # 06
    pDevMode: object  # 07  # LPDEVMODE
    pSepFile: str  # 08
    pPrintProcessor: str  # 09
    pDatatype: str  # 10
    pParameters: str  # 11
    pSecurityDescriptor: object  # 12  # PSECURITY_DESCRIPTO
    Attributes: int  # 13
    Priority: int  # 14
    DefaultPriority: int  # 15
    StartTime: int  # 16
    UntilTime: int  # 17
    Status: PrinterStatus  # 18 # int # PrinterStatus
    cJobs: int  # 19
    AveragePPM: int  # 20

    @staticmethod
    def from_printer(printer_handle):
        attributes = win32print.GetPrinter(printer_handle)
        return PRINTER_INFO_2(*attributes)

    def get_status(self) -> PrinterStatus:
        return PrinterStatus(self.Status)

    def get_attribute(self) -> PrinterAttribute:
        return PrinterAttribute(self.Attributes)

    pass
