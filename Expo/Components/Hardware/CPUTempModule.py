from pathlib import Path

import clr

from Expo.Components.Interfaces.PeriodicModule import PeriodicModule


class CPUTempModule(PeriodicModule):
    _period: float

    def __init__(self, name: str, period: float) -> None:
        super().__init__(name)
        self.c = None
        self._period = period
        self.initialize_openhardwaremonitor()

    def initialize_openhardwaremonitor(self):
        clr.AddReference(str(Path('Libs/OpenHardwareMonitorLib.dll').absolute()))

        from OpenHardwareMonitor.Hardware import Computer
        self.c = Computer()
        self.c.CPUEnabled = True
        self.c.GPUEnabled = True
        self.c.RAMEnabled = True
        self.c.HDDEnabled = True
        self.c.Open()

    def get_cpu_temp(self, c):
        for hw in c.Hardware:
            hw.Update()
            for sensor in hw.Sensors:
                if sensor.SensorType == 2:
                    return sensor.get_Value()
        pass

    def _get_next_time_delta(self) -> float:
        return self._period

    def _on_triggered(self, message_send: callable):
        val = self.get_cpu_temp(self.c)
        if val is None:
            self.status = False, 'Can`t read cpu temperature'
            print('Can`t read cpu temperature')
            return
        message_send(val)
        pass


