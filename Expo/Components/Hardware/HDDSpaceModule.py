import ctypes
import os
import platform

from Expo.Components.Interfaces.PeriodicModule import PeriodicModule


class HDDSpaceModule(PeriodicModule):
    _path: str
    _period: float

    def __init__(self, name: str, path: str, period: float) -> None:
        super().__init__(name)
        self._path = path
        self._period = period

    def get_free_space_mb(self):
        """Return folder/drive free space (in megabytes)."""
        if platform.system() == 'Windows':
            free_bytes = ctypes.c_ulonglong(0)
            ctypes.windll.kernel32.GetDiskFreeSpaceExW(
                ctypes.c_wchar_p(self._path),
                None,
                None,
                ctypes.pointer(free_bytes)
            )
            return free_bytes.value
        else:
            st = os.statvfs(self._path)
            return st.f_bavail * st.f_frsize

    def _get_next_time_delta(self) -> float:
        return self._period

    def _on_triggered(self, message_send: callable):
        val = self.get_free_space_mb()
        if val is None:
            print('Can`t read free space')
            return
        message_send(val)
        pass
