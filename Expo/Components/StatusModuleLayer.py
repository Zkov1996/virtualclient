import os
import platform
from typing import List

from Expo.Components.Decorators.ModuleOnMessageDecorator import on_message_with_type, OnMessageInfo
from Expo.Components.Interfaces.Layer import Layer
from Expo.Components.Interfaces.Module import Module


class StatusModuleLayer(Layer, Module):
    _isOn: bool = False
    _isService: bool = False

    _can_shutdown: bool = False

    _next_trig_time: float = 0
    _interval_time: float = 60

    def __init__(self, name: str, enabled_on_start: bool = False, can_shutdown: bool = False) -> None:
        super().__init__(name)
        self._isOn = enabled_on_start
        self._can_shutdown = can_shutdown

    def pass_send_message(self, module: Module, topic: str, payload: str) -> bool:
        return super().pass_send_message(module, topic, payload) and (module == self or self._isOn or self._isService)

    def pass_receive_message(self, module: Module, topic: str, payload: str) -> bool:
        return super().pass_receive_message(module, topic, payload) and (
                module == self or self._isOn or self._isService)

    def _collect_commands(self) -> List[OnMessageInfo]:
        return super()._collect_commands() + [self.on_receive_power, self.on_receive_service]

    @on_message_with_type('POWER', 'BOOL_S', 1)
    def on_receive_power(self, payload: str):
        self._isOn = payload == '1'
        if not self._isOn and self._can_shutdown:
            self._shutdown()
        pass

    @on_message_with_type('SERVICE', 'BOOL_S', 1)
    def on_receive_service(self, payload: str):
        self._isService = bool(payload) is True
        pass

    def update(self, time_from_start_seconds: float, message_send: callable):
        super().update(time_from_start_seconds, message_send)

        if self._next_trig_time <= time_from_start_seconds:
            self._next_trig_time = time_from_start_seconds + self._interval_time
            message_send('SERVICE' if self._isService else 'ON' if self._isOn else 'OFF')
            pass

    def _shutdown(self):
        if platform.system() == 'Windows':
            os.system('shutdown -s')
        else:
            os.system('systemctl poweroff')
        pass
