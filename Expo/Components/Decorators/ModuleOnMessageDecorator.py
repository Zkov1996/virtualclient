class OnMessageInfo:
    topic: str
    qos: int
    func: object

    def __init__(self, message: str, message_type: str, qos: int, func: object) -> None:
        super().__init__()

        self.topic = message_type + '/' + message if message_type is not None else message
        self.qos = qos
        self.func = func


def on_message(message: str, qos: int):
    def on_message_decorator(fn) -> OnMessageInfo:
        def wrapper(self, payload: str):
            return fn(self, payload)

        return OnMessageInfo(message, None, qos, wrapper)

    return on_message_decorator


def on_message_with_type(message: str, message_type: str, qos: int):
    def on_message_decorator(fn) -> OnMessageInfo:
        def wrapper(self, payload: str):
            return fn(self, payload)

        return OnMessageInfo(message, message_type, qos, wrapper)

    return on_message_decorator
