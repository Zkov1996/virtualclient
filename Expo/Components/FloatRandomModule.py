import random
from math import sin
from typing import Tuple

from Expo.Components.Interfaces.PeriodicModule import PeriodicModule


class FloatRandomModule(PeriodicModule):
    _seconds_interval: Tuple[float, float]

    _next_trig_time: float = 0

    def __init__(self, name: str, seconds_min: float, seconds_max: float) -> None:
        super().__init__(name)
        self._seconds_interval = (seconds_min, seconds_max)

    def update(self, time_from_start_seconds: float, message_send: callable):
        super().update(time_from_start_seconds, message_send)
        if self._next_trig_time <= time_from_start_seconds:
            self._next_trig_time = time_from_start_seconds + self._get_next_time_delta()
            message_send(sin(time_from_start_seconds))
            pass

    def _get_next_time_delta(self) -> float:
        return random.uniform(self._seconds_interval[0], self._seconds_interval[1])
        pass
