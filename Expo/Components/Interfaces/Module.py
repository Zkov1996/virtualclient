from typing import List

from Expo.Components.Decorators.ModuleOnMessageDecorator import OnMessageInfo
from Expo.Components.Interfaces.Component import Component


class Module(Component):
    _commands: List[OnMessageInfo] = []
    name: str
    status: (bool, str)

    def __init__(self, name: str) -> None:
        super().__init__()
        self._commands = self._collect_commands()
        self.name = name

    def get_status(self) -> (bool, str):
        return True, 'OK'

    def update(self, time_from_start_seconds: float, message_send: callable):
        pass

    def _collect_commands(self) -> List[OnMessageInfo]:
        return []

    def get_commands(self) -> List[OnMessageInfo]:
        return self._commands
