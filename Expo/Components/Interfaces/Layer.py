from Expo.Components.Interfaces.Component import Component
from Expo.Components.Interfaces.Module import Module


class Layer(Component):

    def pass_send_message(self, module: Module, topic: str, payload: str) -> bool:
        return True

    def pass_receive_message(self, module: Module, topic: str, payload: str) -> bool:
        return True

    def pass_module_update(self, module: Module) -> bool:
        return True

    # def TransformSubscribeTopic(self, topic: str):
    #     return topic
