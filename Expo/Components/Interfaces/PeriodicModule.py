from Expo.Components.Interfaces.Module import Module


class PeriodicModule(Module):

    _next_trig_time: float = 0

    def __init__(self, name: str) -> None:
        super().__init__(name)

    def update(self, time_from_start_seconds: float, message_send: callable):
        super().update(time_from_start_seconds, message_send)
        if self._next_trig_time <= time_from_start_seconds:
            self._next_trig_time = time_from_start_seconds + self._get_next_time_delta()
            self._on_triggered(message_send)
            pass

    def _get_next_time_delta(self) -> float:
        pass

    def _on_triggered(self, message_send: callable):
        pass
