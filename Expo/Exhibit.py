from typing import List

from Expo.Components.Interfaces.Component import Component
from Expo.Components.Interfaces.Layer import Layer
from Expo.Components.Interfaces.Module import Module


class Exhibit:
    topic: str
    node: str = None
    _layers: List[Layer]
    _modules: List[Module]

    def __init__(self, topic: str, node: str, components: List[Component]) -> None:
        super().__init__()
        self.topic = topic
        self.node = node
        self._layers = [layer for layer in components if isinstance(layer, Layer)]
        self._modules = [module for module in components if isinstance(module, Module)]

    def get_layers(self) -> List[Layer]:
        return self._layers

    def get_modules(self) -> List[Module]:
        return self._modules
