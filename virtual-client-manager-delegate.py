import signal
import queue
from threading import Event

import paho.mqtt.client as mqtt

from Configures.Config import Config
from Configures.ConfigLoader import ConfigLoader
from Configures.FakeConfig import FakeConfig
from asmuron_lib import conf as Conf, payload as Payload

from VirtualClientManager import VirtualClientManager

"""
Демо делегат по обновлению HTML экспонатов

Что делает:
1. при получении mqtt команды CONTENT_UPDATE_CONTROL UPDATE с update_type=html
2. создает symlink с именем current на контент
3. отправляет mqtt команду REBOOT с топиком обновленного экспоната

Делегат организует очередь команд чтобы на блокировать приемку новых.
Обработка команд из очереди (как и публикация mqtt) проводится в основном потоке.
Приемка mqtt и поддержание коннекта происходит в альтернативном потоке
"""

config: Config = ConfigLoader.load('Config.json')
# config: Config = FakeConfig()
termEvent = Event()

mqtt_client = mqtt.Client(config.mqtt.client.device)
virtualClientManager = VirtualClientManager(mqtt_client, config, termEvent)


def SIGINT_handler(signum, frame):
    print('<SIGINT>')
    # termEvent.set()
    termEvent.set()


print("virtual-client-manager started")
signal.signal(signal.SIGINT, SIGINT_handler)

# client.on_Log = on_Log # may be used for debug
mqtt_client.on_connect = virtualClientManager.on_connected
mqtt_client.on_message = virtualClientManager.on_message

print("connecting to {}:{}".format(config.mqtt.broker.host, config.mqtt.broker.port))

while not mqtt_client.is_connected() and not termEvent.wait(0):
    try:
        mqtt_client.connect(config.mqtt.broker.host, config.mqtt.broker.port, 60)
        mqtt_client.loop_start()
        while not mqtt_client.is_connected() and not termEvent.wait(0):
            pass
    except ConnectionRefusedError:
        print('Can`t connect to broker at {}:{}\nReconnecting'.format(config.mqtt.broker.host, config.mqtt.broker.port))
        pass

virtualClientManager.start()

mqtt_client.disconnect()
mqtt_client.loop_stop()

print("done.")
